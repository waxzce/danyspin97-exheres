# Copyright 2020 Danilo Spinella <danyspin97@protonmail.com>
# Copyright 2020 Mykola Orliuk <virkony@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require github [ user='ziglang' ] cmake

myexparam llvm_version=11

export_exlib_phases src_install

SUMMARY="General-purpose programming language and toolchain"
HOMEPAGE="https://ziglang.org"

LICENCES="MIT"
SLOT="0"

MYOPTIONS=""

LLVM_SLOT=$(exparam llvm_version)

DEPENDENCIES="
    build+run:
        dev-lang/clang:${LLVM_SLOT}
        dev-lang/llvm:${LLVM_SLOT}
        sys-devel/lld:${LLVM_SLOT}
"

LLVM_DIR=lib/llvm/${LLVM_SLOT}

CMAKE_SRC_CONFIGURE_PARAMS=(
    # libclang-cpp.so is not symlinked in /usr/lib, linking phase will fail
    -DZIG_PREFER_CLANG_CPP_DYLIB=false
    -DZIG_STATIC:BOOL=FALSE
    -DZIG_TEST_COVERAGE:BOOL=FALSE
    -DLLVM_CONFIG_EXE=/usr/$(exhost --target)/${LLVM_DIR}/bin/llvm-config
    -DLLD_INCLUDE_DIRS=/usr/$(exhost --target)/${LLVM_DIR}/include/
    -DLLD_LIBDIRS=/usr/$(exhost --target)/${LLVM_DIR}/lib
    -DCLANG_LIBDIRS=/usr/$(exhost --target)/${LLVM_DIR}/lib
)

zig_src_install() {
    cmake_src_install

    keepdir /usr/$(exhost --target)/lib/${PN}/std/net
    keepdir /usr/$(exhost --target)/lib/${PN}/std/unicode
}

